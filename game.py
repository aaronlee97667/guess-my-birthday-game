from random import randint

player_name = input("What is your name?\n")

guess_number = 0

for attempts in range(5):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)
    print("Guess" + str(attempts+1) + " : " + player_name + " were you born on " + str(month_number) + " / " + str(year_number) + " ?")
    answer = input("Yes or no?\n")

    if(answer in ["Yes", "yes"]):
        print("I knew it!")
        exit()
    else:
        if(guess_number >= 4):
            print("I have other things to do goodbye!")
            exit()
        else:
            print("Drat! Lemme Try Again!")
            guess_number += 1
